=========================
Personal Project Template
=========================

This is a personal project template that I like to use when starting new projects these days.  It is based on the excellent cookiecutter-pylibrary_.

I find it works well for both library and application projects because it automatically adds a command line entry point for the module
that I develop.  If it is a library, then I add the requirements to the setup.py.


.. _cookiecutter-pylibrary: https://github.com/ionelmc/cookiecutter-pylibrary

